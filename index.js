const { Client, Intents, DiscordAPIError, MessageEmbed, Message } = require("discord.js");
const dotenv = require("dotenv");
const moment = require("moment");

const mongoose = require("mongoose");
const { ServerApiVersion } = require('mongodb')
const cli = require("nodemon/lib/cli");
const usersSchema = require('./model/user');

let checkCollection1 = true;
let checkCollection2 = true;
let uesr1;
let user2;
const idToken = "OTA4MjYzNzE3MTUyNjQ5MjE4.YYzMgA._BgQ4z3DhODD6p-whAGCG_uRybE";
const idBot = "908263717152649218";
const idTalkRoom = "936142534256787466";
const idQueueRoom = "933000751914041385";
const idCheckNameRoom = "930009718229966908";

let dateDay = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' }));
let hours = dateDay.getHours();
let min = dateDay.getMinutes();
let sec = dateDay.getSeconds();
let dateNewMo = dateDay.getDate() + '/' + (dateDay.getMonth() + 1) + '/' + dateDay.getFullYear() + " เช้า";
let dateNewEv = dateDay.getDate() + '/' + (dateDay.getMonth() + 1) + '/' + dateDay.getFullYear() + " บ่าย";
let todayDate = dateDay.getDate();

dotenv.config();

//Database//
const url = `mongodb+srv://oanoanfc159:oanfc159@mydb.0gums.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1, connectTimeoutMS: 50000, socketTimeoutMS: 50000 }, () => console.log('Database Connected'));

//Bot//
const client = new Client({
  intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES],
});

//startBot//
client.on("ready", () => {
  console.log("Your bot is ready");
  console.log(`${client.user.tag} has logged in.`);
  client.user.setActivity("พิมพ์ help เพื่อขอความช่วยเหลือ", { type: "WATCHING" });
  const exampleEmbed = new MessageEmbed()
    .setColor('#ffe5f5')
    .setTitle("ต้องการความช่วยเหลือหรอ ฉันมาช่วยแล้ว!")
    .setThumbnail('https://i.imgur.com/2QyWdZm.png')
    .addFields(
      { name: 'ต้องการลิ้งค์เว็บไหนเลือกได้เลย!', value: '▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔' },
      { name: 'เว็บมหาวิทยาลัยบูรพา', value: '✅ พิมพ์คำสั่ง buu' },
      { name: 'เว็บ Buu Net Support', value: '✅ พิมพ์คำสั่ง buu_net' },
      { name: 'เว็บ MyDI', value: '✅ พิมพ์คำสั่ง myid' },
      { name: 'facebook สำนักคอมพิวเตอร์', value: '✅ พิมพ์คำสั่ง fb_combuu' },
      { name: 'facebook สำนักงานการศึกษา คณะวิทยาการสารสนเทศ', value: '✅ พิมพ์คำสั่ง fb_fois' },
      { name: 'เว็บคณะวิทยการสารสนเทศ', value: '✅ พิมพ์คำสั่ง fois_buu' },
      { name: '\u200B', value: '\u200B' },

    )
    .addFields(
      { name: 'หากคุณต้องการเช็คชื่อล่ะก็ พิมพ์ ci รหัสนิสิต ในห้องเช็คชื่อซิ!(ตย.ci 61160168 , เฉพาะนิสิต)', value: '▔▔▔▔▔▔▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔▔▔▔▔▔▔' },
      { name: '\u200B', value: '\u200B' },
    )
    .addFields(
      { name: 'คำสั่งต่างๆในห้องคิว', value: '▔▔▔▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔▔▔▔' },
      { name: 'ต้องการจะเริ่มสร้างคิว?', value: '✅ พิมพ์คำสั่ง queue จำนวนกลุ่มที่ต้องการจะเปิดให้ลงทะเบียน(เฉพาะอาจารย์)' },
      { name: 'ต้องการแสดงคิวทั้งหมด?', value: '✅ พิมพ์คำสั่ง show' },
      { name: 'ต้องการแก้ไขชื่อกลุ่ม?', value: '✅ พิมพ์คำสั่ง modq ลำดับที่กลุ่ม ชื่อกลุ่มใหม่' },
      { name: 'ต้องการลบกลุุ่ม?', value: '✅ พิมพ์คำสั่ง delq ลำดับที่กลุ่ม' },
      { name: 'ต้องการลบกลุ่มทั้งหมด?', value: '✅ พิมพ์คำสั่ง delall' },
      { name: 'ต้องการเพิ้่มกลุ่มใหม่?', value: '✅ พิมพ์คำสั่ง addq ชื่อกลุ่มใหม่' }
    )
    .setTimestamp()
  client.channels.cache.get(idTalkRoom).send({ embeds: [exampleEmbed] });
});

function newcreateCollection(collectionName, model) {
  return mongoose.model(collectionName, model);
}

//เช็คชื่อ//
client.on("messageCreate", async msg => {
  const username = msg.author.username;
  const id = msg.author.discriminator;
  const text = msg.content;
  const currentChannel = msg.channel.id;
  const arrayKey = text.split(" ");

  if (username !== "Classroom_Manager" && id !== idBot && currentChannel == idCheckNameRoom) {
    std = parseInt(arrayKey[1]);
    nanCkeck = Number.isNaN(std);
    if (nanCkeck == true) {
      msg.reply("เช็คชื่อผิดพลาด รหัสนิสิตต้องเป็นตัวเลขเท่านั้น ทำการเช็คชื่อใหม่ ใช้คำสั่ง ci รหัสนิสิต");
    } else if (nanCkeck == false) {
      stdCheck = std.toString();
      if (stdCheck.length < 8 || stdCheck.length > 8) {
        msg.reply("เช็คชื่อผิดพลาด รหัสนิสิตต้องมี 8 ตัว ทำการเช็คชื่อใหม่ ใช้คำสั่ง ci รหัสนิสิต");
      } else if (stdCheck.length == 8) {
        switch (arrayKey[0]) {
          case "ci":
            if (dateDay.getHours() != new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getHours()) {
              hours = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getHours();
            }
            if (dateDay.getMinutes() != new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getMinutes()) {
              min = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getMinutes();
            }
            if (dateDay.getSeconds() != new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getSeconds()) {
              sec = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getSeconds();
            }
            if (hours < 12) {
              if (todayDate != new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getDate()) {
                todayDate = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getDate();
                checkCollection1 = true;
                dateDay = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' }));
                dateNewMo = dateDay.getDate() + '/' + (dateDay.getMonth() + 1) + '/' + dateDay.getFullYear() + " เช้า";
              }
              if (checkCollection1 == true) {
                user1 = newcreateCollection(dateNewMo, usersSchema);
                checkCollection1 = false;
              }
              user1.create({
                STD: std,
                name: username,
                userId: id,
                startWork: hours + ':' + min + ':' + sec + " น."
              });
              msg.reply('เช็คชื่อสำเร็จ มาเรียนเวลา : ' + hours + ':' + min + ':' + sec + " น.");
            } else if (hours > 12) {
              if (todayDate != new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getDate()) {
                todayDate = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })).getDate();
                checkCollection2 = true;
                dateDay = new Date(new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' }));
                dateNewEv = dateDay.getDate() + '/' + (dateDay.getMonth() + 1) + '/' + dateDay.getFullYear() + " บ่าย";
              }
              if (checkCollection2 == true) {
                user2 = newcreateCollection(dateNewEv, usersSchema);
                checkCollection2 = false;
              }
              user2.create({
                STD: std,
                name: username,
                userId: id,
                startWork: hours + ':' + min + ':' + sec + " น."
              });
              msg.reply('เช็คชื่อสำเร็จ มาเรียนเวลา : ' + hours + ':' + min + ':' + sec + " น.");
            } else if (hours == 12) {
              msg.reply("ไม่สามารถเช็คชื่อได้ เนื่องจากเวลา 12.00 - 12.59 น. เป็นเวลาพัก");
            }
            break;
          default:
            msg.reply("เช็คชื่อผิดพลาด ลองใหม่อีกครั้ง ทำการเช็คชื่อใช้คำสั่ง ci รหัสนิสิต");
            break;
        }
      }
    }
  }
});

//แนบลิ้งค์//
client.on("messageCreate", msg => {
  if (msg.content == "buu") {
    msg.reply("https://reg.buu.ac.th/registrar/home.asp");
  }
  if (msg.content == "buu_net") {
    msg.reply("https://netsupport.buu.ac.th");
  }
  if (msg.content == "myid") {
    msg.reply("https://myid.buu.ac.th");
  }
  if (msg.content == "fb_combuu") {
    msg.reply("https://www.facebook.com/computerburapha");
  }
  if (msg.content == "fb_fois") {
    msg.reply("https://www.facebook.com/%E0%B8%AA%E0%B8%B3%E0%B8%99%E0%B8%B1%E0%B8%81%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%A8%E0%B8%B6%E0%B8%81%E0%B8%A9%E0%B8%B2-%E0%B8%84%E0%B8%93%E0%B8%B0%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%AA%E0%B8%B2%E0%B8%A3%E0%B8%AA%E0%B8%99%E0%B9%80%E0%B8%97%E0%B8%A8-1332636130169022/");
  }
  if (msg.content == "fois_buu") {
    msg.reply("https://www.informatics.buu.ac.th/2020/");
  }
  if (msg.content == "help") {
    const exampleEmbed = new MessageEmbed()
      .setColor('#ffe5f5')
      .setTitle("ต้องการความช่วยเหลือหรอ ฉันมาช่วยแล้ว!")
      .setThumbnail('https://i.imgur.com/2QyWdZm.png')
      .addFields(
        { name: 'ต้องการลิ้งค์เว็บไหนเลือกได้เลย!', value: '▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔' },
        { name: 'เว็บมหาวิทยาลัยบูรพา', value: '✅ พิมพ์คำสั่ง buu' },
        { name: 'เว็บ Buu Net Support', value: '✅ พิมพ์คำสั่ง buu_net' },
        { name: 'เว็บ MyDI', value: '✅ พิมพ์คำสั่ง myid' },
        { name: 'facebook สำนักคอมพิวเตอร์', value: '✅ พิมพ์คำสั่ง fb_combuu' },
        { name: 'facebook สำนักงานการศึกษา คณะวิทยาการสารสนเทศ', value: '✅ พิมพ์คำสั่ง fb_fois' },
        { name: 'เว็บคณะวิทยการสารสนเทศ', value: '✅ พิมพ์คำสั่ง fois_buu' },
        { name: '\u200B', value: '\u200B' },

      )
      .addFields(
        { name: 'หากคุณต้องการเช็คชื่อล่ะก็ พิมพ์ ci รหัสนิสิต ในห้องเช็คชื่อซิ!(ตย.ci 61160168 , เฉพาะนิสิต)', value: '▔▔▔▔▔▔▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔▔▔▔▔▔▔' },
        { name: '\u200B', value: '\u200B' },
      )
      .addFields(
        { name: 'คำสั่งต่างๆในห้องคิว', value: '▔▔▔▔▔▔▔▔◥ 🧡 ◤▔▔▔▔▔▔▔▔▔' },
        { name: 'ต้องการจะเริ่มสร้างคิว?', value: '✅ พิมพ์คำสั่ง queue จำนวนกลุ่มที่ต้องการจะเปิดให้ลงทะเบียน(เฉพาะอาจารย์)' },
        { name: 'ต้องการแสดงคิวทั้งหมด?', value: '✅ พิมพ์คำสั่ง show' },
        { name: 'ต้องการแก้ไขชื่อกลุ่ม?', value: '✅ พิมพ์คำสั่ง modq ลำดับที่กลุ่ม ชื่อกลุ่มใหม่' },
        { name: 'ต้องการลบกลุุ่ม?', value: '✅ พิมพ์คำสั่ง delq ลำดับที่กลุ่ม' },
        { name: 'ต้องการลบกลุ่มทั้งหมด?', value: '✅ พิมพ์คำสั่ง delall' },
        { name: 'ต้องการเพิ้่มกลุ่มใหม่?', value: '✅ พิมพ์คำสั่ง addq ชื่อกลุ่มใหม่' }
      )
      .setTimestamp()
    client.channels.cache.get(idTalkRoom).send({ embeds: [exampleEmbed] });
  }
});

//คิว//
var queueSlot = [];
var queueInt = 0;
var count = 0;
var check = 0;
var orderGroup = 0;
var numcheck = 0;
var NumberOfQueue = 0;
var allNumQueue = 0;
let NewNumberofQueue;
client.on("messageCreate", (msg) => {
  const username = msg.author.username;
  const id = msg.author.discriminator;
  const text = msg.content;
  const currentChannel = msg.channel.id;
  if (
    username !== "Classroom_Manager" &&
    id !== idBot &&
    currentChannel == idQueueRoom
  ) {
    const myArray = msg.content.split(" ");
    if (myArray[0] == "queue" && myArray[1]) {
      orderGroup++;
      msg.reply("กลุ่มที่เปิดให้ลงทะเบียนมีทั้งหมด " + myArray[1] + " กลุ่ม");
      msg.reply("โปรดใส่ชื่อกลุ่มที่ " + orderGroup);
      queueSlot = [];
      queueInt = parseInt(myArray[1]);
      check = parseInt(myArray[1]);
    } else if (queueInt > 0) {
      queueSlot.push(msg.content);
      queueInt--;
      orderGroup++;
      allNumQueue++;
      msg.reply("ลงคิวสำเร็จ");
      count++;
      if (count < check) {
        msg.reply("โปรดใส่ชื่อกลุ่มที่ " + orderGroup);
      }
      if (count == check) {
        msg.reply(
          "ลงครบตามจำนวนที่กำหนดแล้ว พิมพ์คำสั่ง show เพื่อแสดงคิวทั้งหมด"
        );
        orderGroup = 0;
        count = 0;
      }
    } else if (msg.content == "show") {
      const exampleEmbedQueue = new MessageEmbed()
        .setColor("#ffe5f5")
        .setTitle("รายการคิวของวันนี้มีทั้งหมด " + allNumQueue + " กลุ่ม")
        .setTimestamp()
        .setThumbnail("https://i.imgur.com/UgA4rXh.png");
      for (let i = 0; i < queueSlot.length; i++) {
        const num = (i + 1).toString() + ".";
        exampleEmbedQueue.addField(num.toString(), queueSlot[i]);
      }
      client.channels.cache
        .get(idQueueRoom)
        .send({ embeds: [exampleEmbedQueue] });
    } else if (myArray[0] == "modq" && myArray[1] && myArray[2]) {
      if (myArray[1] > queueSlot.length) {
        msg.reply("ไม่พบกลุ่มที่ต้องการแก้ไข");
      } else {
        myArray[1]--;
        oldName = queueSlot[myArray[1]];
        queueSlot[myArray[1]] = myArray[2];
        newName = queueSlot[myArray[1]] = myArray[2];
        msg.reply(
          "แก้ไขชื่อกลุ่มที่ " +
          (myArray[1] + 1) +
          " " +
          oldName +
          " --> " +
          newName
        );
        msg.reply("สามารถพิมพ์คำสั่ง show เพื่อแสดงคิวทั้งหมดอีกครั้ง")
      }
    } else if (myArray[0] == "modq" && myArray[1]) {
      msg.reply("คำสั่งไม่ถูกต้อง กรุณาป้อนคำสั่งให้ถูกด้อง");
    } else if (myArray[0] == "delq" && myArray[1]) {
      if (myArray[1] > queueSlot.length) {
        msg.reply("ไม่พบกลุ่มที่ต้องการลบ");
      } else {
        myArray[1]--;
        const index = queueSlot.indexOf(queueSlot[myArray[1]]);
        if (index > -1) {
          queueSlot.splice(index, 1);
        }
        NewNumberofQueue = allNumQueue - queueSlot.length;
        msg.reply("ลบกลุ่มที่ " + (myArray[1] + 1) + " สำเร็จ");
        msg.reply("ตอนนี้มีคิวว่าง " + NewNumberofQueue + " คิว" + " สามารถใช้คำสั่ง addq เพื่อเพิ่มกลุ่มใหม่ได้");
      }
    } else if (myArray[0] == "delall") {
      if (queueSlot.length <= 0) {
        msg.reply("ไม่พบกลุ่มที่ต้องการลบ");
      } else {
        queueSlot = [];
        msg.reply("ลบกลุ่มทั้งหมดแล้ว");
        msg.reply("ตอนนี้มีคิวว่าง " + allNumQueue + " คิว" + " สามารถใช้คำสั่ง addq เพื่อเพิ่มกลุ่มใหม่ได้");
      }
    } else if (myArray[0] == "addq" && myArray[1]) {
      if (allNumQueue == queueSlot.length) {
        msg.reply("จำนวนกลุ่มเต็มแล้ว ไม่สามารถเพิ่มได้");
      } else {
        queueSlot.push(myArray[1]);
        msg.reply("เพิ่มกลุ่มใหม่แล้ว");
        if (allNumQueue == queueSlot.length) {
          msg.reply("จำนวนกลุ่มเต็มแล้ว ไม่สามารถเพิ่มได้อีก");
          msg.reply("สามารถพิมพ์คำสั่ง show เพื่อแสดงคิวทั้งหมดอีกครั้ง")
        }
      }
    } else if (
      myArray[0] != "queue" ||
      myArray[0] != "show" ||
      myArray[0] != "modq" ||
      myArray[0] != "delq" ||
      myArray[0] != "delall" ||
      myArray[0] != "addq"
    ) {
      msg.reply("คำสั่งผิดพลาด โปรดพิมพ์คำสั่งใหม่");
    }
  }
});

client.login(idToken);
